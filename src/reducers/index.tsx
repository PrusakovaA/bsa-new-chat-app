import { combineReducers } from "redux";
import chat from "../components/Chat/reducer"
import message from "../components/Message/reducer"
import Routing from '../components/Login/reducer';  
import Users from '../components/UsersList/reducer';  

const rootReducer = combineReducers({
    chat,
    message,
    login: Routing,
    users: Users,
});

export default rootReducer;