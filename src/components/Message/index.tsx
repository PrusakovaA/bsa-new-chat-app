import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import { connect } from "react-redux";
import Message from "../../types/IMessage";
import { changeLike } from "../Chat/actions";
import OwnMessage from "./OwnMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import "./index.css";

type IMessageProps = {
  message: Message;
  userId: string;
  changeLike: (id: string, isLike: boolean) => void;
}

const MessageClass = (props: IMessageProps) => {
  const {
    message, changeLike,  userId } = props;
  const [isLike, setIsLike] = useState(false);
  const history = useHistory();

  const onLikeMessage = () => {
    changeLike(message.id, !isLike);
    setIsLike(!isLike);
  };

  const getUsersMessages = (message: Message, id: string) => {
      if (message.userId === userId) {
        return (
          <OwnMessage
            key={id}
            message={message}
          />
        );
      } else {
        return (<div className="others-message">
        <div className="message-content">
        <div className="message-user-avatar">
            <img
              className="avatar"
              src={props.message.avatar}
              alt="avatar"
            />
          </div>
          <div className="text">
        <div className="message-text">{props.message.text}</div>
        </div>
          <div className="message-info">
          <span className="message-user-name">{props.message.user}</span>
            <span className="message-time">{props.message.time}</span>
            <div className="like" onClick={() => onLikeMessage()}>
              <span>{isLike ? <FontAwesomeIcon icon={ faHeart} className="message-liked"/> 
              : <FontAwesomeIcon icon={ faHeart} className="message-like"/>}</span>
          </div>
          </div>
      </div>  
      </div>
        );
      }
    }

      return( <div className="message">
       {getUsersMessages(props.message, props.message.id)}
      </div>
      );
}

const mapDispatchToProps = {
changeLike
};

export default connect(null, mapDispatchToProps)(MessageClass);