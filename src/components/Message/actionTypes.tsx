export enum OwnMessageAction {
    SET_MESSAGE_ID = "SET_MESSAGE_ID",
    SET_EDITED = "SET_EDITED",
    SHOW_MODAL = "SHOW_MODAL",
    HIDE_MODAL = "HIDE_MODAL",
  }