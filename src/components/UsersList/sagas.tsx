import {
    call, put, takeEvery, all, 
  } from 'redux-saga/effects'
import { addUsersList, deleteUsersList, fetchUsersList, updateUsersList } from '../../services/user.service';
  import {
    ADD_USER, UPDATE_USER, DELETE_USER, FETCH_USERS,
    AddUserAction, UpdateUserAction, DeleteUserAction, FETCH_USERS_SUCCESS, ERROR,
  } from './actionTypes';
  import {UsersList} from '../../types/User';
  
  export function* fetchUsers() {
    try {
      const users: UsersList  = yield call(fetchUsersList);
      yield put({ type: FETCH_USERS_SUCCESS, payload: users });
    } catch (error) {
      yield put({ type: ERROR, payload: error.message });
    }
  }
  
  function* watchFetchUsers() {
    yield takeEvery(FETCH_USERS, fetchUsers);
  }
  
  export function* addUser(action: AddUserAction) {
    const newUser = { ...action.payload.data };
  
    try {
      yield call(addUsersList, newUser);
      yield put({ type: FETCH_USERS });
    } catch (error) {
      yield put({ type: ERROR, payload: error.message });
    }
  }
  
  function* watchAddUser() {
    yield takeEvery(ADD_USER, addUser);
  }
  
  export function* updateUser(action: UpdateUserAction) {
    const { id } = action.payload;
    const updatedUser = { ...action.payload.data };
  
    try {
      yield call(updateUsersList, updatedUser, id);
      yield put({ type: FETCH_USERS });
    } catch (error) {
      yield put({ type: ERROR, payload: error.message });
    }
  }
  
  function* watchUpdateUser() {
    yield takeEvery(UPDATE_USER, updateUser);
  }
  
  export function* deleteUser(action: DeleteUserAction) {
    try {
      yield call(deleteUsersList,action.payload.id);
      yield put({ type: FETCH_USERS });
    } catch (error) {
      yield put({ type: ERROR, payload: error.message });
    }
  }
  
  function* watchDeleteUser() {
    yield takeEvery(DELETE_USER, deleteUser);
  }
  
  export default function* usersSagas() {
    yield all([
      watchFetchUsers(),
      watchAddUser(),
      watchUpdateUser(),
      watchDeleteUser(),
    ]);
  }