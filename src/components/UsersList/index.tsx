import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import Preloader from '../Preloader';
import * as actions from './actions';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import { UsersList } from '../../types/User';
import UserItem from './UserItem';
import './index.css';


const mapStateToProps = (state: { users: UsersList}) => ({
    users: state.users.users,
    isLoading: state.users.isLoading,
    error: state.users.error,
  });
  
  const mapDispatchToProps = {
    ...actions,
  };
  
  type Props = ReturnType<typeof mapStateToProps> &
      typeof mapDispatchToProps & RouteComponentProps
  
  class UserList extends Component<Props, {}> {
    constructor(props: Props) {
      super(props);
      this.onEdit = this.onEdit.bind(this);
      this.onDelete = this.onDelete.bind(this);
      this.onAdd = this.onAdd.bind(this);
      this.onChat = this.onChat.bind(this);
    }
  
    componentDidMount() {
      const { fetchUsers } = this.props;
      fetchUsers();
    }
  
    onEdit(id: string) {
      const { history } = this.props;
      history.push(`/user/${id}`);
    }
  
    onDelete(id: string) {
      const { deleteUser } = this.props;
      deleteUser(id);
    }
  
    onAdd() {
      const { history } = this.props;
      history.push('/user');
    }
  
    onChat() {
      const { history } = this.props;
      history.push('/chat');
    }
  
    render() {
      const {
        users, isLoading, history, error,
      } = this.props;
      if (isLoading) {
        return <Preloader />;
      }
      if (error) {
        history.push('/error');
      }
      return (
        <div className="users-list">
          <div className="title-buttons">
            <Button
              type="button"
              onClick={this.onAdd}
            >
              Add user
            </Button>
            <Button
              type="button"
              onClick={this.onChat}
            >
              Chat
            </Button>
          </div>
          <ListGroup className="list-group">
            {
             users.map((user) => (
               <UserItem
                 key={user.id}
                 id={user.id}
                 username={user.username}
                 email={user.email}
                 onEdit={this.onEdit}
                 onDelete={this.onDelete}
               />
             ))
            }
          </ListGroup>
        </div>
      );
    }
  }
  
  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserList));