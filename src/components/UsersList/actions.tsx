import {
    ADD_USER, UPDATE_USER, DELETE_USER, FETCH_USERS,
  } from './actionTypes';
  import service from '../../services/user.service';
  import { UserData } from '../../types/User';
  
  export const addUser = (data: UserData) => ({
    type: ADD_USER,
    payload: {
      id: service.getNewId(),
      data,
    },
  });
  
  export const updateUser = (id: string, data: UserData) => ({
    type: UPDATE_USER,
    payload: {
      id,
      data,
    },
  });
  
  export const deleteUser = (id: string) => ({
    type: DELETE_USER,
    payload: {
      id,
    },
  });
  
  export const fetchUsers = () => ({
    type: FETCH_USERS,
  });
