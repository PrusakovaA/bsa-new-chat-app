import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";

type Props = {
    id: string;
    username: string;
    email: string;
    onEdit: (id: string) => void;
    onDelete: (id: string) => void;
}

const UserItem = ({
  id, username, email, onEdit, onDelete,
}: Props) => (
  <ListGroup.Item>
    <div>
      <div className="profile-data">
        <span>
          {username}
        </span>
        <span>{email}</span>
      <div>
        <Button type="button" onClick={() => onEdit(id)}> Edit </Button>
        <Button type="button" onClick={() => onDelete(id)}> Delete </Button>
      </div>
      </div>
    </div>
  </ListGroup.Item>
);

export default UserItem;