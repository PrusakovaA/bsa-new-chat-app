import React,  {
  MutableRefObject, useEffect, useRef, useState,
}  from "react";
import { connect } from "react-redux";
import { useParams, useHistory, withRouter } from 'react-router-dom';
import { editMessage } from "../Chat/actions";
import { showModal } from "../Message/actions";
import Message from "../../types/IMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import "./index.css";


type Props = ReturnType<typeof mapStateToProps> &
    typeof mapDispatchToProps

const EditModal = (props: Props) => {
  const {
    message,
    editMessage,
    showModal: load,
  } = props;
  const [body, setBody] = useState('');
  const inputRef = useRef() as MutableRefObject<Text>;
  const { id } = useParams<{ id: string }>();
  const history = useHistory();

  useEffect(() => {
    load();
  }, [load]);

  useEffect(() => {
    if (message) setBody(message.text);
  }, [message, inputRef]);

  const handleCancel = () => {
    setBody('');
    history.push('/chat');
  };

  const handleEdit = () => {
    if (!body || !message) {
      handleCancel();
      return;
    }
    const text: string = body;
    if (text.length === 0) return;
    editMessage(message.id, text);
    handleCancel();
  };

    return (
      <div className="edit-message-modal">
        <div className="modal-shown">
          <div className="modal-header">
            <span>Edit Message</span>
          </div>
          <div className="modal-body">
            <div className="edit-message-input">
              <input
                type="text"
                className="edit-text-area"
                value={body}
              />
              <div className="buttons">
              <button className="edit-message-button" onClick={handleEdit}>
              <FontAwesomeIcon icon={faCheck} />
              </button>
              <button
                className="edit-message-close"
                onClick={handleCancel}
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
              </div>
            </div>
          </div>
        </div>
        </div>
    );
  }

const mapStateToProps = (state: { message: {message: Message} }) => ({
  message: state.message.message,
});

const mapDispatchToProps = {
  showModal,
  editMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);