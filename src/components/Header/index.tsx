import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComments } from '@fortawesome/free-solid-svg-icons';
import { faUsers } from '@fortawesome/free-solid-svg-icons';
import {faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons';
import "./index.css";
interface IHeaderProps {
    title: string;
    usersCount: number;
    messagesCount: number;
    lastMessageDate: string;
}

const Header = ({ title, usersCount,  messagesCount, lastMessageDate}: IHeaderProps) => {
        return (
            <div className="chat-header">
            <div className="header-title">{title}
            <FontAwesomeIcon icon={faComments} className="chatIcon"/>
            </div>
            <div className="chat-info">
                <span className="header-users-count">{usersCount} participants <FontAwesomeIcon icon={faUsers} /></span>
                <span className="header-messages-count">{messagesCount} messages <FontAwesomeIcon icon={faEnvelopeOpen} /></span>
            </div>
            <div className="last-message">
                    <span className="header-last-message-date">last message at: {lastMessageDate}</span>
                </div>
        </div>
        );
    };

    Header.propTypes = {
        title: PropTypes.string,
        usersCount: PropTypes.number,
        messagesCount: PropTypes.number,
        lastMessageDate: PropTypes.string,
    };

export default Header;