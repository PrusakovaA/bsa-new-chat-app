import React, {
  useState, MutableRefObject, useRef, useEffect,
} from "react";
import { connect } from "react-redux";
import { addMessage } from "../Chat/actions";
import {setMessageId, showModal} from "../Message/actions";
import data from "../../services/DataService";
import Message from "../../types/IMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import './index.css';

type IMessageInputProps = {
  messages: Message[];
  addMessage: Function;
  setMessageId: Function;
  showModal: Function;
  userId: string;
}
const MessageInput = (props: IMessageInputProps) => {
  const {  addMessage, userId } = props;
  const [body, setBody] = useState('');
  const messagesEndRef = useRef() as MutableRefObject<HTMLDivElement>;

  const onSend = () => {
        const date: Date = new Date();
        const message: Message = {
          id: date.getTime().toString(),
          user: "Me",
          userId: userId,
          text: body,
          createdAt: date,
          time: data.getTime(date),
          likesCount: 0
        };
        props.addMessage(message);
      }
     
      useEffect(() => {
        messagesEndRef.current.scrollIntoView({ block: 'end', behavior: 'smooth' });
      }, [messagesEndRef]);

        return (
          <div className="message-input">
            <form className="message-input-text">
              <input
                type="text"
                className="text-area"
                placeholder="Message"
                value={body}
                onChange={(evt) => setBody((evt.target as unknown as React.FormEvent<HTMLInputElement>).currentTarget.value)}
              />
            </form>
            <button className="message-input-button"
              onClick={() => onSend()}
            >
              <FontAwesomeIcon icon={faPaperPlane} className="send"/>
            </button>
          </div>
        );
      }

    interface Store {
      chat: {
        messages?: Message[];
      };
    }
    
    const mapStateToProps = (state: Store) => {
      return {
        messages: state.chat.messages!,
      };
    };
    
    const mapDispatchToProps = {
      addMessage,
      showModal,
      setMessageId
    };
    
    export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);