import {
    Redirect, Route, Switch, withRouter, useHistory,
  } from 'react-router-dom';
import Routing from "./Routes/Routing";
function App() {
    return (
      <>
       <Routing />
      </>
    );
  }

export default App;