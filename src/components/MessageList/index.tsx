import React from "react";
import { connect } from "react-redux";
import EditModal from "../EditModal";
import "./index.css";
import Message from "../../types/IMessage";
import MessageClass from "../Message";
import api from "../../services/DataService";

type IMessageListProps = {
    messages: Message[];
    editModal: boolean;
    userId: string;
}

const  MessageList = (props: IMessageListProps) => {
  const {
    messages,  userId,
  } = props;
        return (props.editModal ?
           (<div className="message-list"><EditModal /></div>) : (
          <div className="message-list">
             {api.groupByDate(props.messages).map((groupsByDate, id) => (
          <div className="message-list-group" key={id}>
            <div className="messages-divider">{groupsByDate.date}</div>
            {groupsByDate.messages.map((message: Message, id: string) => (
              <MessageClass
              key={id}
              message={message}
              userId={userId}
              />
            ))}
          </div>
        ))}
          </div>
        ));
}

interface Store {
  message: {
    editModal: boolean;
  };
}

const mapStateToProps = (state: Store) => {
  return {
    editModal: state.message.editModal,
  };
};
export default connect(mapStateToProps)(MessageList);