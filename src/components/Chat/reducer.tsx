import { ChatAction } from "./actionTypes";
import Message from "../../types/IMessage";
import DataService from "../../services/DataService";


interface ChatState {
  preloader: boolean;
    messages?: Message[];
    usersCount: number;
  }


const initialState: ChatState = {
  preloader: true,
    messages: [],
    usersCount: 0
  };

  interface ChatActions {
    type: ChatAction;
    payload?: {
      data?: any;
      id: string;
      messages: Message[];
      usersCount?: number;
    };
  }

  export default function (state = initialState, action: ChatActions) {
    switch (action.type) {
      case ChatAction.INIT: {
        const { messages, usersCount } = action.payload!;
        return {
          messages,
          usersCount,
          preloader: true
        };
      }
      case ChatAction.HIDE_LOADING: {
        return { 
          ...state, 
          preloader: false 
          };
      }
      case ChatAction.CHANGE_LIKE: {
        const { id } = action.payload!;
        const messages = [...state.messages!];
        let message = messages.find((message) => message.id === id);
        message!.likesCount = message!.likesCount ? 0 : 1;
        return { 
          ...state,
           messages 
          };
      }
      case ChatAction.ADD_MESSAGE: {
        const { data } = action.payload!;
        const newMessage: Message = { ...data };
        const messages = [...state.messages!];
        messages.push(newMessage);
        let usersCount = DataService.getUsersCount(messages);
        return { 
          ...state, 
          messages, usersCount
          };
      }
      case ChatAction.DELETE_MESSAGE: {
        const { id } = action.payload!;
        const filteredMessages = state.messages!.filter(
          (message) => message.id !== id
        );
        const usersCount  = DataService.getUsersCount(filteredMessages);
        return { 
          ...state, 
          messages: filteredMessages, 
          usersCount};
      }
      case ChatAction.EDIT_MESSAGE: {
        const { id, data } = action.payload!;
        const updatedMessages = state.messages!.map((message) => {
          if (message.id === id) {
            return {
              ...message,
              ...data,
            };
          } else {
            return message;
          }
        });
        return {
           ...state, 
           messages: updatedMessages 
          };
      }
      default:
        return state;

    }
  }
  