import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { AuthorizedUser } from '../../types/UserLogin';

const PrivateRoute = ({ component: Component, isAuthorized, ...rest }: any) => (
  <Route
    {...rest}
    render={(props) => (isAuthorized
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
);

const mapStateToProps = (state: {login: {isAuthorized: boolean, user: AuthorizedUser}}) => ({
  isAuthorized: state.login.isAuthorized,
});

export default connect(mapStateToProps)(PrivateRoute);