import React, { useEffect } from 'react';
import {
  Redirect, Route, Switch, withRouter, useHistory,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { AuthorizedUser } from '../../types/UserLogin';
import LoginPage from '../Login/index';
import AppHeader from '../AppHeader';
import Chat from '../Chat';
import UserList from '../UsersList';
import UserPage from '../UserEditor';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';

const mapStateToProps = (state: { login: {isAuthorized: boolean, user: AuthorizedUser} }) => ({
  user: state.login.user,
  isAuthorized: state.login.isAuthorized,
});

type Props = ReturnType<typeof mapStateToProps>

const Routing = ({
  user,
  isAuthorized,
}: Props) => {
  const history = useHistory();
  useEffect(() => {
    if (!isAuthorized) {
      history.push('/login');
    }
  }, [history, isAuthorized]);
  return (
    <>
      <AppHeader />
      <Switch>
        <PublicRoute exact path="/login" component={LoginPage} />

        {user.username === 'admin' }
        &&
        (
        <PrivateRoute exact path="/users" component={UserList} />
        <PrivateRoute exact path="/user/:id" component={UserPage} />
        <PrivateRoute exact path="/user" component={UserPage} />
        )

        <PrivateRoute exact path="/chat" component={Chat} />
      </Switch>
    </>
  );
};

export default withRouter(connect(mapStateToProps)(Routing));