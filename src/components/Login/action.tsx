import { UserLogin } from '../../types/UserLogin';
import { LoginAction } from './actionTypes';

export const loginUser = (user: UserLogin) => ({
  type: LoginAction.LOGIN_USER,
  payload: {
    user,
  },
});

export const startLoading = () => ({
  type: LoginAction.START_LOADING,
});