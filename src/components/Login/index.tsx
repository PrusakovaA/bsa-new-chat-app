import React from 'react';
import { connect } from 'react-redux';
import {LoginState} from '../../types/UserLogin';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { loginUser, startLoading } from './action';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter } from 'react-router-dom';
import "./index.css";
import LoginForm from './LoginForm';

const mapStateToProps = (state: { login: LoginState}) => ({
  isLoading: state.login.isLoading,
  error: state.login.error,
});

const mapDispatchToProps = {
  loginUser,
  startLoading,
};

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

const LoginPage = ({
  loginUser: login, startLoading: start, isLoading, error,
}: Props) => (
  <Container fluid>
  <Row>
    <Col>
  <div className="login">
        {error && <p>{error}</p>}
    <LoginForm  login={login}  isLoading={isLoading} startLoading={start}/>
  </div>
  </Col>
  </Row>
</Container>
);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginPage));