import {
    all, call, put, takeEvery 
  } from 'redux-saga/effects';
  import {LoginAction, LoginUserAction}  from './actionTypes';
import { login } from '../../services/login.service';
;
  
  export async function* loginUser(action: LoginUserAction) {
    try {
      const user: Response = yield call (login, action.payload.user);
      yield put({
        type: LoginAction.LOGIN_USER_SUCCESS,
        payload: user.body,
      });
    } catch (error) {
      yield put({ type: LoginAction.LOGIN_USER_ERROR });
    }
  }
  
  function* watchLoginUser() {
    yield takeEvery(LoginAction.LOGIN_USER, loginUser);
  }
  
  export default function* routingSagas() {
    yield all([
      watchLoginUser(),
    ]);
  }