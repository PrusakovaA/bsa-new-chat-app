import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { UserLogin } from "../../types/UserLogin";
import 'bootstrap/dist/css/bootstrap.min.css';
import Spinner from "react-bootstrap/Spinner";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Preloader from '../Preloader';

type Props = {
  login: (user: UserLogin) => void;
  isLoading: boolean;
  startLoading: () => void;
}

const LoginForm = ({ login, isLoading, startLoading }: Props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleUsernameChange = (data: string) => {
    setUsername(data);
  };

  const handlePasswordChange= (data: string) => {
    setPassword(data);
  };

  const handleLoginClick = async () => {
    startLoading();
    login({ username, password });
  };

  return (
    <Form className="login-form" onSubmit={handleLoginClick}>
      <Form.Group  controlId="username">
        <Form.Label>Username</Form.Label>
        <Form.Control
          className="form-username"
          autoFocus
          type="username"
          value={username}
          onChange={(evt) => handleUsernameChange(evt.target.value)}
        />
      </Form.Group>
      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          value={password}
          onChange={(evt) => handlePasswordChange (evt.target.value)}
        />
      </Form.Group>
      <Button className="login-button" block  type="submit" >
        Login
      </Button>
      { isLoading === true &&
         <Preloader/>
      }
    </Form>
  );
  
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
};

export default LoginForm;