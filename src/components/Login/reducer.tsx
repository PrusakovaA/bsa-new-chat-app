import { LoginAction, LoginUserType } from './actionTypes';
import { AuthorizedUser} from '../../types/UserLogin';
  
const initialState = {
  isAuthorized: false,
  user: {} as AuthorizedUser,
  isLoading: false,
  error: '',
};

export default function (state = initialState, action: LoginUserType) {
  switch (action.type) {
    case LoginAction.LOGIN_USER_SUCCESS: {
      return {
        user: action.payload,
        isAuthorized: true,
      };
    }

    case LoginAction.LOGIN_USER_ERROR: {
      return {
        ...state,
        isLoading: false,
        error: 'Invalid username or password',
      };
    }
    case LoginAction.START_LOADING: {
      return {
        ...state,
        isLoading: true,
      };
    }
    default:
      return state;
  }
}