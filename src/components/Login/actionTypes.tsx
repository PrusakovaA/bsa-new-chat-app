import { AuthorizedUser, UserLogin } from "../../types/UserLogin";

export enum LoginAction {
    LOGIN_USER = 'LOGIN_USER',
    LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS',
    LOGIN_USER_ERROR = 'LOGIN_USER_ERROR',
    START_LOADING = 'START_LOADING'
}


export interface LoginUserAction {
    type: typeof LoginAction.LOGIN_USER;
    payload: { user: UserLogin };
}

export interface StartLoadingAction {
    type: typeof LoginAction.START_LOADING;
}

interface LoginSuccessUserAction {
    type: typeof LoginAction.LOGIN_USER_SUCCESS;
    payload: AuthorizedUser;
}

interface LoginErrorUserAction {
    type: typeof LoginAction.LOGIN_USER_ERROR,
}

export type LoginUserType = LoginSuccessUserAction | StartLoadingAction | LoginErrorUserAction;