import { all } from 'redux-saga/effects';
import loginSagas from '../components/Login/sagas';
import usersSagas from '../components/UsersList/sagas';
export default function* rootSaga() {
    yield all([
        loginSagas(),
        usersSagas(),
    ]);
  }