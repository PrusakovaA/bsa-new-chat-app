class DateClass {

    getFormatDate(dateFromData: Date) {
      const today = new Date();
      const yesterday = new Date(today.getDate() - 1);
      const date = new Date(dateFromData);

      if (date.getDay() === today.getDay() &&
          date.getMonth() === today.getMonth() &&
          date.getFullYear() === today.getFullYear()) return "Today";
      else if (date.getDate() === yesterday.getDate() &&
              date.getMonth() === yesterday.getMonth() &&
              date.getFullYear() === yesterday.getFullYear()) return "Yesterday";
      else {
        return new Intl.DateTimeFormat('en-GB', {
          weekday: 'long',
          day: 'numeric',
          month: 'long'
      }).format(date);
      }
    }

}
export default new DateClass();