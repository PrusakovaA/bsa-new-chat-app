import axios from "axios";
import { UserLogin } from "../types/UserLogin";

export function login(user: UserLogin){
    return  axios ({
      method: 'post',
      url: 'http://localhost:3001/login',
      data: {
        username: user.username,
        password: user.password
      }
  });
  }

