import Message from "../types/IMessage";
import dateFormat from './DateFormat';

class DataService {
    groupByDate(messages: Message[]){
      const groups = messages.reduce((groups: any, message: Message) => {
          const date = new Date(message.createdAt);
          const separatorName = dateFormat.getFormatDate(date);
          if (!groups[separatorName]) { groups[separatorName] = []; }
          groups[separatorName].push(message);
          return groups;
        }, {});
      
        const groupArrays = Object.keys(groups).map((date) => {
          return {
            date,
            messages: groups[date],
          };
        });
        return groupArrays;
  }
  
  getTime = (date: string | Date) => {
    const day = new Date(date);
    let minutes = day.getMinutes().toString();
    if (minutes.length === 1) minutes = "0" + minutes;
    return `${day.getHours()}:${minutes}`;
  };

  getUsersCount(messages: Message[] | undefined) {
    if(messages === undefined) return 0;
    const list = new Set();
    messages.map((message) => (
      list.add(message.user)
    ));
    return list.size;
  };
  
}

export default new DataService();