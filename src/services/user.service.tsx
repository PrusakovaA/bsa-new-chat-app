import axios, { AxiosResponse } from "axios";
import { UserData } from "../types/User";

const getNewId = () => (new Date()).getTime().toString();

export default {
  getNewId,
}

export function fetchUsersList(){
  return axios ({
      method: 'get',
      url: 'http://localhost:3001/users'
  }).then( resp => resp.data );
}


export function addUsersList(newUser: UserData){
  return axios ({
    method: 'post',
    url: 'http://localhost:3001/user', 
    data: newUser}).then( resp => resp.data );
}

export function updateUsersList(User: UserData, id: string){
  return axios.put (`http://localhost:3001/user/${id}`, User).then( resp => resp.data );
}

export function deleteUsersList(id: string){
  return  axios ({
    method: 'delete',
    url: `http://localhost:3001/users/${id}`
}).then( res => res);
}