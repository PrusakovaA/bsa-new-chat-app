import {User} from './User';

export type UserLogin = {
    username: string;
    password: string;
}

export type AuthorizedUser = {
    user: User;
    username: string;
}

export type LoginState = {
    isAuthorized: boolean;
    user:  User;
    isLoading: boolean;
    error: string;
   }

   