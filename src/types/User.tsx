export type User = {
    id: string;
    username: string;
    email: string;
    password: string;
    avatar: string;
   }

   export type UserData = {
    username: string;
    email: string;
    password: string;
   }

   export type UsersList = {
    users: User[];
    isLoading: boolean;
    error: string;
   }